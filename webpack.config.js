var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin  = require('html-webpack-plugin');
//var VendorChunkPlugin = require('webpack-vendor-chunk-plugin');
//var CopyWebpackPlugin  = require('copy-webpack-plugin');
var TransferWebpackPlugin  = require('transfer-webpack-plugin');

var PATHS = {
  entry: path.join(__dirname, './src/entry.ts'),
  src: path.join(__dirname, './src'),
  build: path.join(__dirname, './dist'),
  node_modules: path.resolve(__dirname, './node_modules'),
  style: path.resolve(__dirname, './src/style')
};

module.exports = {
  entry: {
    app:PATHS.entry,
    vendors: []
  },

  output: {
    path: './dist',
    filename: '[name].js'
  },

  publicPath: 'dist/',
  devtool: 'source-map',
  debug: true,
  cache: true,

  resolve: {
    extensions: ['', '.js', '.ts', '.webpack.js', '.web.js', 'html']
  },

  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        include: [
          /src/,
          /ng2-bootstrap/
        ],
        exclude: [
          /\.spec\.ts$/,
          /\.e2e\.ts$/,
          /node_modules\/(?!(ng2-bootstrap))/,
          /dist/
        ]
      },
      { test: /\.sass$/, loader: ExtractTextPlugin.extract("style", "raw-loader!sass"), include: PATHS.style },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract("style", "raw-loader!sass"), include: PATHS.style },
      { test: /\.html$/,  loader: 'raw-loader' }
    ]
  },

  plugins:[
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      //inject: 'body',
      hash: true
    }),
    new ExtractTextPlugin("css/style.css"),
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
    new TransferWebpackPlugin([
      { from: './node_modules/font-awesome/fonts/', to: 'fonts' },
      { from: './src/assets/', to: 'assets' },
    ], path.join(__dirname, './'))

  ]
};
