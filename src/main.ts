import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from "./component/app/app.module";

// enableProdMode();
platformBrowserDynamic().bootstrapModule(AppModule);