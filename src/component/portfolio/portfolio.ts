import {Component, Input, ElementRef, OnInit, AfterViewInit} from '@angular/core';
let d3 =  require('d3');

var template:string = require('to-string!./portfolio.tpl.html');

@Component({
  selector: 'portfolio',
  template: template
})

export class PortfolioComponent implements AfterViewInit {

  @Input() protected data:any;
  protected host:any;
  protected svg:any;

  public constructor(protected ref:ElementRef) {
    var el:any = this.ref.nativeElement;
    this.host = d3.select(el);
  }

  ngAfterViewInit() {
    this.initSvg();
  }

  private buildChart() {
    //
  }

  private initSvg() {

    let container = this.host.select('.resume__container');

    let w, h;
    w = container['_groups'][0][0].clientWidth;
    h = 200;

    this.svg = this.host.select('.resume__placeholder').insert('svg', 'div')
      .attr("viewBox", "0 0 " + w + " " + w)
      .attr("preserveAspectRatio", "xMidYMid meet");

    // parse the date / time
    var timeParse = d3.timeParse("%Y%d%m");

    let lineData = [
      {
        value: this.data.initialContribution,
        date: this.data.firstUpdated
      },
      {
        value: this.data.marketValue,
        date: this.data.lastUpdated
      }
    ];

    // set ranges
    let xr = d3.scaleTime().range([0, w]);
    let yr = d3.scaleLinear().range([h, 0]);

    // define line
    let line = d3.line()
      .x(function(d, i) { return xr(d.date); })
      .y(function(d, i) {
        return yr(d.value);
      });

    // format date
    lineData.forEach(function(d) {
      d.date = timeParse(d.date);
    });

    // data range
    xr.domain(d3.extent(lineData, function(d) { return d.date; }));
    yr.domain([0, d3.max(lineData, function(d) { return d.value; })]);

    // line
    this.svg.append("path")
      .data([lineData])
      .attr("class", "line")
      .attr("d", line);

    // x axis
    this.svg.append("g")
      .attr("transform", "translate(0," + h + ")")
      .call(d3.axisBottom(xr));

    // y axis
    this.svg.append("g")
      .call(d3.axisLeft(yr));
  }

  isReturnNegative() {
    return this.data.investmentReturnSinceStartPercentage < 0;
  }

  isReturnPositive() {
    return this.data.investmentReturnSinceStartPercentage > 0;
  }

  parseDate(d:string) {
    let day = d.substring(6, 8);
    let month = d.substring(4, 6);
    let year = d.substring(0, 4);
    let date =  new Date(month + "/" + day + "/" + year);
    return date.getTime();
  }

}
