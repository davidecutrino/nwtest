import { Inject, Component, NgModule, ModuleWithProviders, ChangeDetectorRef} from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { Http, HttpModule, ConnectionBackend } from '@angular/http';
import {LocationStrategy, PathLocationStrategy, HashLocationStrategy} from '@angular/common';
import { AppComponent } from './app.component';
import {Service} from "../../service/service";
import {PortfolioComponent} from "../portfolio/portfolio";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    AppComponent,
    PortfolioComponent
  ],
  providers: [
    // Config,
    Service,
  ],

  exports: [],
  bootstrap: [ AppComponent ]
})

export class AppModule {}