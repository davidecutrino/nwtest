import {Component} from '@angular/core';
import {Service} from "../../service/service";

var template:string = require('to-string!./app.tpl.html');

@Component({
  selector: 'nw-test',
  template: template
})

export class AppComponent {

  private data:Array<any>;

  public constructor(protected service:Service) {

    this.data = this.service.getUserSummaryData();
    console.log(this.data);
  }

  public objKeys = function (o:Object):Array<string> {
    return Object.keys(o);
  }
}
